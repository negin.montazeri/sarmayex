<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=
            [
                [
                    'id'=>1,
                    'username' => 'NeginMontazeri',
                    'password'=>bcrypt('123456'),
                    'role'=>'Admin',
                    'created_at' => date("Y-m-d H:i:s"),

                ],
                [
                    'id'=>2,
                    'username' => 'VidaKhaledi',
                    'password'=>bcrypt('123456'),
                    'role'=>'Member',
                    'created_at' => date("Y-m-d H:i:s"),
                ],
                [
                    'id'=>3,
                    'username' => 'RamtinMontazeri',
                    'password'=>bcrypt('123456'),
                    'role'=>'Member',
                    'created_at' => date("Y-m-d H:i:s"),
                ],


            ];

        DB::table('users')->insert($data);
    }
}
