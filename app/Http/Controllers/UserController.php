<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Resources\TaskCollection;
use App\Models\Task;
use App\Models\TaskUser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserCollection;

class UserController extends Controller
{
    public function login(LoginUserRequest $request)
    {
        $validated = $request->validated();
        $user=User::where('username',$validated['username'])->first();
        if (Hash::Check($validated['password'], $user->password)) {
            $token = $user->createToken('admin');
            $data['token'] = $token->accessToken;
            $data['username'] = $user->username;
                return $data;
        }
         else {
             return response()->json(['error' => 'Username or password is incorrect'],401);
        }
    }
    public function returnUsers()
    {

        if (Gate::allows('isAdmin'))
            return new UserCollection(User::all());
        else
            return response()->json(['error' => 'You do not have permission'],401);
    }

    public function returnTasks()
    {
        if (Gate::allows('isAdmin'))
        {
            $tasks = Task::with('users')->get();
            return new TaskCollection($tasks);
        }
        else
            return response()->json(['error' => 'You do not have permission'],401);
    }

    public function storeTask(StoreTaskRequest $request)
    {
        $validated = $request->validated();
        $validated["user_id"] = $request->user()->id;
        $task = Task::firstOrCreate($validated);
        $this->assignTask($task->id,$request->user()->id);
        return response()->json(['data' =>'Task created successfully'] ,200);

    }

    private function assignTask($taskId,$userId){
        TaskUser::firstOrCreate(['task_id'=>$taskId,'user_id'=>$userId]);
    }


    public function destroyTask(Task $task)
    {
        if (Gate::allows('isAdmin'))
        {
            $task->delete();
        }
        else
            return response()->json(['error' => 'You do not have permission'],401);
        return response()->json(['data' =>'The task has been deleted'] ,200);
    }

    public function updateTask(UpdateTaskRequest $request,Task $task)
    {
        if (Gate::allows('isAdmin'))
        {
            $validated = $request->validated();
            $task->update($validated);
        }
        else
            return response()->json(['error' => 'You do not have permission'],401);
        return response()->json(['data' =>'The task has been updated'] ,200);
    }

    public function assignAdminToTask(Task $task)
    {
        if (Gate::allows('isAdmin'))
        {

            $this->assignTask($task->id,Auth::user()->id);
        }
        else
            return response()->json(['error' => 'You do not have permission'],401);
        return response()->json(['data' =>'The task was assigned to you'] ,200);


    }

}

