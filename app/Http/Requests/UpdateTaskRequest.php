<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string|max:50',
            'description' => 'string|max:300',
        ];
    }
    public function messages()
    {
        return [
            'title.max'=>'Please enter no more than 50 characters for title',
            'description.max'=>'Please enter no more than 300 characters for description'
        ];
    }
}
