<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'string|required|max:50|exists:users,username',
            'password' => 'string|required|max:50',
        ];
    }
    public function messages()
    {
        return [
            'username.exists'=>'The username is not found',
            'username.required' => 'Username is required',
            'password.required' => 'Password is required'
        ];
    }
}
