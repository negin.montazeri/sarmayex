<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class StoreTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string|required|max:50',
            'description' => 'string|required|max:300',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'title.max'=>'Please enter no more than 50 characters for title',
            'description.required' => 'A description is required',
            'description.max'=>'Please enter no more than 300 characters for description'
            ];
    }
}
