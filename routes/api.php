<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login',[UserController::class,'login']);
Route::post('user/task',[UserController::class,'storeTask'])->middleware('auth:user');
Route::prefix('/admin')->middleware('auth:user')->group(function (){
    Route::get('/users',[UserController::class,'returnUsers']);
    Route::prefix('/task')->group(function (){
        Route::delete('/{task}',[UserController::class,'destroyTask']);
        Route::get('/',[UserController::class,'returnTasks']);
        Route::put('/{task}',[UserController::class,'updateTask']);
        Route::post('/{task}',[UserController::class,'assignAdminToTask']);
    });
});

